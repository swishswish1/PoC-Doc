// Template for this file was automatically generated according to graphQL schema.
// The following block is subject to replacement on module runtime upload
// and should be maintained unchanged (including blanks).

// {
const isFromWeb = false;
let typePaths = [];
let path = '';
__dirname = '';
// }

if (isFromWeb)
  process.chdir(__dirname);

import { Module, UseInterceptors, UseGuards } from '../../node_modules/@nestjs/common';
import {
  Resolver,
  Query,
  Mutation
  Args,
  ResolveField,
  Parent,
  Context,
} from '../../node_modules/@nestjs/graphql';
import { ConfigService } from '../../node_modules/config-lib';
import { BaseResolver } from '../base-classes/base.resolver';
import { SqlModule } from './sql/sql.module';
import { NeoModule } from './neo/neo.module';
import { NeoService } from './neo/neo.service';
import { SqlService } from './sql/sql.service';
import { ExecutionContextValidator } from '../common/execution-context-validator';
import { logger } from '../../node_modules/logger-lib';
import { AuthModule, GqlAuthGuard } from '../../node_modules/auth-lib';
import { DirHolder } from '../modules-tools/dir-holder';
const { BasicValidationInterceptor, DurationInterceptor } = require('../../node_modules/interceptors-lib');

if (isFromWeb) {
  process.chdir(DirHolder.getProjectDir());
else {
  const configService = new ConfigService();
  const urlJoin = require('url-join');
  typePaths = [urlJoin(configService.get('GQL_URL'), configService.get('GQL_SCHEMA'))];
  path = configService.get('GQL_PATH');
}

const gql_module_lib = 'gql-module-lib';
const { GraphQLModuleEx } = require(isFromWeb ? `./node_modules/${gql_module_lib}` : gql_module_lib);

///////////////////////////////////////////////////////////////////////////////////////////////////////


@Resolver('Person')
export class PersonResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,ListType,NonNullType,NamedType,Person
  @Query()
  @UseGuards(GqlAuthGuard)
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new BasicValidationInterceptor(new ExecutionContextValidator()))
  async allPersons() {

    return [];  //@@
  }

  // Args:   {id: NonNullType,NamedType,String}
  // Return: NamedType,Person
  @Query()
  @UseGuards(GqlAuthGuard)
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new BasicValidationInterceptor(new ExecutionContextValidator()))
  async personById(@Args('id') id: any) {

    return [];  //@@
  }

  // Args:   {surname: NonNullType,NamedType,String}
  // Return: NamedType,Person
  @Query()
  @UseGuards(GqlAuthGuard)
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new BasicValidationInterceptor(new ExecutionContextValidator()))
  async personBySurname(@Args('surname') surname: any) {

    return [];  //@@
  }

  // Args:   {relationQueryArg: NonNullType,ListType,NamedType,RelationQueryArg}
  // Return: ListType,NamedType,Person
  @Query()
  @UseGuards(GqlAuthGuard)
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new BasicValidationInterceptor(new ExecutionContextValidator()))
  async personsByRelation(@Args('relationQueryArg') relationQueryArg: any[]) {

    return [];  //@@
  }

  // Args:   {personsInput: NonNullType,ListType,NamedType,PersonInput}
  // Return: NamedType,String
  @Mutation()
  @UseGuards(GqlAuthGuard)
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new BasicValidationInterceptor(new ExecutionContextValidator()))
  async createPersons(@Args('personsInput') personsInput: any[]): Promise<string> {

    return '';  //@@
  }

  // Args:   {organization: NamedType,String},
  //         {role: NamedType,String},
  //         {since: NamedType,IntInput}
  // Return: ListType,NamedType,Affiliation
  @ResolveField('affiliations')
  @UseInterceptors(DurationInterceptor)
  async affiliations(@Parent() parent: any, 
			@Args('organization') organization: any, 
			@Args('role') role: any, 
			@Args('since') since: any) {

    return [];  //@@
  }

  // Args:   {kind: NamedType,String}
  // Return: ListType,NamedType,Relation
  @ResolveField('relations')
  @UseInterceptors(DurationInterceptor)
  async relations(@Parent() parent: any, 
			@Args('kind') kind: any) {

    return [];  //@@
  }
}

@Resolver('Organization')
export class OrganizationResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,ListType,NonNullType,NamedType,Organization
  @Query()
  @UseGuards(GqlAuthGuard)
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new BasicValidationInterceptor(new ExecutionContextValidator()))
  async allOrganizations() {

    return [];  //@@
  }

  // Args:   
  // Return: NamedType,Organization
  @ResolveField('parent')
  @UseInterceptors(DurationInterceptor)
  async parent(@Parent() parent: any) {

    return [];  //@@
  }
}

@Resolver('Affiliation')
export class AffiliationResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,NamedType,Organization
  @ResolveField('organization')
  @UseInterceptors(DurationInterceptor)
  async organization(@Parent() parent: any) {

    return [];  //@@
  }

  // Args:   
  // Return: NamedType,Role
  @ResolveField('role')
  @UseInterceptors(DurationInterceptor)
  async role(@Parent() parent: any) {

    return [];  //@@
  }
}

@Resolver('Relation')
export class RelationResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,NamedType,Person
  @ResolveField('p1')
  @UseInterceptors(DurationInterceptor)
  async p1(@Parent() parent: any) {

    return [];  //@@
  }

  // Args:   
  // Return: NonNullType,NamedType,Person
  @ResolveField('p2')
  @UseInterceptors(DurationInterceptor)
  async p2(@Parent() parent: any) {

    return [];  //@@
  }
}

@Module({
  imports: [
    GraphQLModuleEx.forRoot({
      debug: false,
      playground: true,
      typePaths,
      path,
      context: ({ req }) => ({ req }),
    }),
    AuthModule,
    SqlModule,
    NeoModule
  ],
  providers: [
			PersonResolver,
			OrganizationResolver,
			AffiliationResolver,
			RelationResolver,
			]
})
export class GqlModule {
  constructor() {
    logger.log('GqlModule has been created');
  }

  onModuleInit() {
    logger.log('GqlModule has been initialized');
  }
}

export function getModule() { return GqlModule; }
